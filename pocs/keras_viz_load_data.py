from keras.datasets import cifar10
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow import keras

(X_train, y_train), (X_test, y_test) = cifar10.load_data()

# Preprocessing

# Scale images to the [0, 1] range
X_train = X_train.astype("float32") / 255
X_test = X_test.astype("float32") / 255
# Make sure images have shape (28, 28, 1)
# X_train = np.expand_dims(X_train, -1)
# X_test = np.expand_dims(X_test, -1)
print("X_train shape:", X_train.shape)
print(X_train.shape[0], "train samples")
print(X_test.shape[0], "test samples")

# convert class vectors to binary class matrices --> One hot encoding
num_classes = 10
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

# (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
plt.imshow(X_train[0])
plt.show()
print(X_train.shape)
# ----

# Implementación de la CNN

"""
Dudas:

*   ¿Porqué una MLP de 1024? ¿Es por algún motivo en especial? ¿Relación de neurona por pixel?
*   No entiendo porqué las capas de convolución tienen las funciones de activación relu
*   Entiendo que el número de épocas las elijo yo, no? Entiendo que en cada época se eligen aletoriamente
    ,de los datos que yo tengo para entrenar, bloques de entrenamiento del tamaño del batch_size.
*   ¿Cómo sabemos el número de épocas necesarias?
*   No se cuantas neuronas ponerle al perceptrón y en que basar esas cuentas

"""
Nconv1 = 3  # Tamaño del filtro convolucional 1 (píxeles)
Nconv2 = 3  # Tamaño del filtro convolucional 2 (píxeles)
Nfil1 = 32  # Número de filtros de la primera etapa
Nfil2 = 64  # Número de filtros de la segunda etapa
Nneu = 1024  # Número de neuronas del MLP final

#tf.keras.backend.clear_session()
input_shape = (32,32,3)
model = tf.keras.Sequential(
    [
        keras.Input(shape=input_shape),
        # tf.keras.layers.Reshape(input_shape=(Npix*Npix,), target_shape=(Npix, Npix, 1)),
        tf.keras.layers.Conv2D(kernel_size=Nconv1, filters=Nfil1, activation='relu', padding='same'),
        tf.keras.layers.MaxPooling2D((2, 2)),
        tf.keras.layers.Conv2D(kernel_size=Nconv2, filters=Nfil2, activation='relu', padding='same'),
        tf.keras.layers.MaxPooling2D((2, 2)),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(Nneu, activation='relu'),
        # tf.keras.layers.Dropout(pDO),
        tf.keras.layers.Dense(10, activation='softmax')
    ])
model.compile(loss="categorical_crossentropy", optimizer=tf.keras.optimizers.Adam(lr=0.01), metrics=["accuracy"])
model.summary()

batch_size = 128
epochs = 15

model.fit(X_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          validation_split=0,
          # validation_data=(xValD, eVal),
          initial_epoch=0,
          )
