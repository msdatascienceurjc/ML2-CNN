from keras.datasets import cifar10
from matplotlib import pyplot as plt
import numpy as np
import tensorflow as tf
from tensorflow import keras
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score






import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'

(X_train, y_train), (X_test, y_test) = cifar10.load_data()

# Preprocessing

# Scale images to the [0, 1] range
X_train = X_train.astype("float32") / 255
X_test = X_test.astype("float32") / 255

X_train,X_valid,y_train,y_valid = train_test_split(X_train, y_train, test_size=0.2, random_state=123, stratify = y_train)

# Make sure images have shape (28, 28, 1)
# X_train = np.expand_dims(X_train, -1)
# X_test = np.expand_dims(X_test, -1)
print("X_train shape:", X_train.shape)
print("x_test shape:", X_test.shape)
print("x_valid shape:", X_valid.shape)
print(X_train.shape[0], "train samples")
print(X_test.shape[0], "test samples")
print(X_valid.shape[0], "validation samples")



# convert class vectors to binary class matrices --> One hot encoding
num_classes = 10
y_trainE = keras.utils.to_categorical(y_train, num_classes)
y_testE = keras.utils.to_categorical(y_test, num_classes)
y_validE = keras.utils.to_categorical(y_valid, num_classes)

#(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
#plt.imshow(X_train[0])
#plt.imshow(X_test[0])
#plt.imshow(X_valid[0])
#plt.show()

print(y_trainE)
# ----



# Implementación de la CNN

"""
Dudas:
 
*   ¿Porqué una MLP de 1024? ¿Es por algún motivo en especial? ¿Relación de neurona por pixel?
*   No entiendo porqué las capas de convolución tienen las funciones de activación relu
*   Entiendo que el número de épocas las elijo yo, no? Entiendo que en cada época se eligen aletoriamente
    ,de los datos que yo tengo para entrenar, bloques de entrenamiento del tamaño del batch_size.
*   ¿Cómo sabemos el número de épocas necesarias?
*   No se cuantas neuronas ponerle al perceptrón y en que basar esas cuentas
"""


Nconv1 = 3  # Tamaño del filtro convolucional 1 (píxeles)
Nconv2 = 3  # Tamaño del filtro convolucional 2 (píxeles)
Nfil1 = 32  # Número de filtros de la primera etapa
Nfil2 = 64  # Número de filtros de la segunda etapa
Nneu = 160  # Número de neuronas del MLP final

act_layer = tf.keras.layers.LeakyReLU(alpha=0.4)

#tf.keras.backend.clear_session()
input_shape = (32,32,3)
model = tf.keras.Sequential(
    [
        keras.Input(shape=input_shape),
        # tf.keras.layers.Reshape(input_shape=(Npix*Npix,), target_shape=(Npix, Npix, 1)),
        tf.keras.layers.Conv2D(kernel_size=Nconv1, filters=Nfil1, activation=act_layer, padding='same'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Conv2D(kernel_size=Nconv1, filters=Nfil1, activation=act_layer, padding='same'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPooling2D((2, 2)),
        tf.keras.layers.Dropout(0.2),
        #-----------------------------------------------
        tf.keras.layers.Conv2D(kernel_size=Nconv2, filters=Nfil2, activation=act_layer, padding='same'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Conv2D(kernel_size=Nconv2, filters=Nfil2, activation=act_layer, padding='same'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.MaxPooling2D((2, 2)),
        tf.keras.layers.Dropout(0.4),
        #-----------------------------------------------
        tf.keras.layers.Flatten(),
        #-----------------------------------------------
        tf.keras.layers.Dense(Nneu, activation=act_layer,kernel_initializer='he_uniform'),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Dropout(0.5),
        tf.keras.layers.Dense(10, activation='softmax')
    ])


model.compile(loss="categorical_crossentropy", optimizer=tf.keras.optimizers.Adam(lr=0.01), metrics=["accuracy"])
model.summary()


batch_size = 80
epochs = 40


history = model.fit(X_train, y_trainE, batch_size=batch_size, epochs=epochs,
                    validation_data=(X_test, y_testE),initial_epoch=0)


otrain=model.predict(X_train)
otest=model.predict(X_test)
ovalid=model.predict(X_valid)

ytrainD=otrain.argmax(axis=1)
ytestD=otest.argmax(axis=1)
yvalidD=ovalid.argmax(axis=1)

pAciertoTrain=accuracy_score(y_train,ytrainD)
pAciertoTest=accuracy_score(y_test,ytestD)
pAciertoValid=accuracy_score(y_valid,yvalidD)

print("Probabilidad de acierto (Train): %s"%(pAciertoTrain))

#Probabilidad de acierto (Train): 0.8311
print("Probabilidad de acierto (Test): %s"%(pAciertoTest))
#Probabilidad de acierto (Test): 0.7375
print("Probabilidad de acierto (Validation): %s"%(pAciertoValid))
#Probabilidad de acierto (Validation): 0.7391





"""
plt.plot(history.history['acc'], label='accuracy')
plt.plot(history.history['val_acc'], label = 'val_accuracy')
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1])
plt.legend(loc='lower right')
plt.show()
test_loss, test_acc = model.evaluate(X_test, y_test, verbose=2)

print(test_acc)
"""