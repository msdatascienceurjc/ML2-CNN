# ML2 CNN

Space to work on Convolutional Neural network (CNN) for image classification for animals and vehicles.

Libraries used: Tensorflow and Scikitlearn

## Prerequisites

`conda create --name <name_env> python=3.6`
`conda activate <name_env>`
`pip --version`

## Installation

`pip install tensorflow keras`